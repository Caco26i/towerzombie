﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeInteractive
{
    public class FoodPoints : MonoBehaviour
    {
        public float life;
        public float energy;

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.tag == "Player")
            {
                Player player = collider.gameObject.GetComponent<Player>();
                player.health += life;
                player.energy += energy;

                transform.position = player.transform.position;
                transform.Translate(new Vector3(0, 1, 0));

                transform.parent = player.transform;
                player.actualFood = gameObject;
            }
        }
    }
}