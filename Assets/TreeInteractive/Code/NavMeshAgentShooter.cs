﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


namespace TreeInteractive
{
    public class NavMeshAgentShooter : MonoBehaviour
    {
        public string TargetTag = "";
        private NavMeshAgent _agent;
        private Tower _tower;

        //[SerializeField] private Animator _animator;
        // Start is called before the first frame update
        void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            GameObject tower = GameObject.FindWithTag(TargetTag);
            _tower = tower.GetComponent<Tower>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_tower.CurrentTarget != null)
            {
                _agent.SetDestination(_tower.CurrentTarget.transform.position);
            }
        }
    }
}