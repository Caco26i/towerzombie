﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeInteractive.Model
{
    [CreateAssetMenu(fileName = "New Zombie", menuName = "Zombie/Zombie Type")]
    public class Zombie : ScriptableObject
    {
        public Vector2 speed2;
        public float speed;
        public float angularSpeed;
        public float Acceleration;
        [Range(0.3f, 0.5f)]
        public float minRadio;
        [Range(0.5f, 1f)]
        public float maxRadio;
        public GameObject prefab;

        public GameObject InstanciateZombie(Vector3 pos, Quaternion rot)
        {
            GameObject newZombie = Instantiate(prefab, pos, rot);
            UnityEngine.AI.NavMeshAgent zombieAgent = newZombie.GetComponent<UnityEngine.AI.NavMeshAgent>();

            Animator animator = newZombie.GetComponent<Animator>();
            animator.speed = UnityEngine.Random.Range(0.8f, 1.2f);

            zombieAgent.speed = speed;

            CapsuleCollider collider = newZombie.GetComponent<CapsuleCollider>();
            zombieAgent.radius = 0.5f;
            return newZombie;
        }
    }
}