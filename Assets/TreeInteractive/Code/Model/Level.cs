﻿using System.Collections;
using System.Collections.Generic;
using TreeInteractive.Util;
using UnityEngine;

namespace TreeInteractive.Model
{
    [CreateAssetMenu(fileName = "New Level", menuName = "Level/Level Config")]
    public class Level : ScriptableObject
    {
        public float numZombies;
        public Zombie[] zombies;

        [Range(0f, 1f)]
        public float percentFood;
        public Food[] food;

        [Range(0f, 1f)]
        public float percentObstacles;
        public GameObject[] obstacles;

        public GameObject InstantiateZombie(Vector3 pos, Quaternion rot)
        {
            Zombie zombieConf = zombies.RandomElement();
            return zombieConf.InstanciateZombie(pos, rot);
        }

        public GameObject InstantiateObstacle(Vector3 pos, Quaternion rot, float scale)
        {
            GameObject newObstacle = Instantiate(obstacles.RandomElement(), pos, rot);
            newObstacle.transform.localScale = new Vector3(scale, scale, scale);
            return newObstacle;
        }

        public GameObject InstantiateFood(Vector3 pos, Quaternion rot, float scale)
        {
            Food foodConf = food.RandomElement();
            return foodConf.InstanciateFood(pos, rot);
        }
    }
}