﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeInteractive.Model
{
    [CreateAssetMenu(fileName = "New Food", menuName = "Food/Food Type")]
    public class Food : ScriptableObject
    {
        [Range(0.01f, 0.2f)]
        public float life;
        [Range(0.01f, 0.2f)]
        public float energy;
        public GameObject prefab;

        public GameObject InstanciateFood(Vector3 pos, Quaternion rot)
        {
            GameObject newFood = Instantiate(prefab, pos, rot);
            FoodPoints points = newFood.GetComponent<FoodPoints>();
            points.energy = energy;
            points.life = life;
            return newFood;
        }
    }
}