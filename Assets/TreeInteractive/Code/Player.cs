﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeInteractive
{
    public class Player : MonoBehaviour
    {
        public float health;
        public float energy;
        public GameObject actualFood;
    }
}