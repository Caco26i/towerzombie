﻿using System.Collections;
using System.Collections.Generic;
using TreeInteractive.Model;
using UnityEngine;

namespace TreeInteractive
{
    public class GameManager : MonoBehaviour
    {
        public Level[] levels;
        public int maxLevel = 10;
        public LevelManager levelManager;
        public Transform[] spaws;

        // Start is called before the first frame update
        void Start()
        {
            InitialiseGame();
        }

        // Update is called once per frame
        void Update()
        {

        }

        void InitialiseGame()
        {
            levelManager.InitialiseList();
            Level actualLevel = levels[levelManager.level];
            levelManager.AddObstacles(actualLevel);
            levelManager.AddFood(actualLevel);
            levelManager.AddZombies(spaws, actualLevel);
        }
    }
}