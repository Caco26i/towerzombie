﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


namespace TreeInteractive
{
    public class NavMeshAgentEnemy : MonoBehaviour
    {
        public string TargetTag = "";
        public NavMeshAgent _agent;
        private GameObject _target;

        [SerializeField] private Animator _animator;

        // Start is called before the first frame update
        void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            _target = GameObject.FindWithTag(TargetTag);
            _agent.SetDestination(_target.transform.position);

        }

        // Update is called once per frame
        void Update()
        {
            // prevent x / 0
            if (_agent.velocity.magnitude == 0)
                _animator.SetFloat("MoveSpeed", _agent.velocity.magnitude);
            else                               // max speed / magnitude (max = 1)
                _animator.SetFloat("MoveSpeed", _agent.speed / _agent.velocity.magnitude);

        }
    }
}