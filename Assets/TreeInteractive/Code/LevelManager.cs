﻿using System.Collections;
using System.Collections.Generic;
using TreeInteractive.Model;
using TreeInteractive.Util;
using UnityEngine;

namespace TreeInteractive
{
    public class LevelManager : MonoBehaviour
    {
        public int level = 0;
        public int columns = 10;                                    //Number of columns in our game board.
        public int rows = 10;                                       //Number of rows in our game board.
        public int widthTile = 10;                                  //Size of the Tile
        public int offset = 25;
        public GameObject prefab;
     
        private List<Vector3> _gridPositions = new List<Vector3>();  //A list of possible locations to place tiles.

        public void InitialiseList()
        {
            _gridPositions.Clear();

            for (int x = 1; x < columns; x++)
            {
                for (int z = 1; z < rows; z++)
                {
                    if (x != columns / 2 && z != rows / 2) //don't put at the middle
                        _gridPositions.Add(new Vector3(x * widthTile - offset, 0f, z * widthTile - offset));
                }
            }
        }

        public void AddObstacles(Level level)
        {
            foreach (Vector3 pos in _gridPositions)
            {

                float rand = Random.Range(0f, 1f);

                if (rand < level.percentObstacles)
                {
                    Vector3 auxPos = new Vector3(pos.x + Random.Range(-widthTile / 2, widthTile / 2), pos.y, pos.z + Random.Range(-widthTile / 2, widthTile / 2));
                    float randScale = Random.Range(0.5f, 1);
                    Quaternion randRot = Quaternion.Euler(0, Random.Range(0, 360), 0);

                    level.InstantiateObstacle(auxPos, randRot, randScale);

                    randScale = Random.Range(0.5f, 1);
                    auxPos = new Vector3(auxPos.x + Random.Range(-widthTile / 2, widthTile / 2), auxPos.y, auxPos.z + Random.Range(-widthTile / 2, widthTile / 2));

                    level.InstantiateObstacle(auxPos, randRot, randScale);
                }
            }
        }

        public void AddFood(Level level)
        {
            foreach (Vector3 pos in _gridPositions)
            {
                float rand = Random.Range(0f, 1f);
                if (rand < level.percentObstacles)
                {
                    Vector3 auxPos = new Vector3(pos.x + Random.Range(-widthTile / 2, widthTile / 2), pos.y, pos.z + Random.Range(-widthTile / 2, widthTile / 2));
                    float randScale = Random.Range(0.5f, 1);
                    Quaternion randRot = Quaternion.Euler(0, Random.Range(0, 360), 0);

                    level.InstantiateFood(auxPos, randRot, randScale);
                }
            }
        }

        public void AddZombies(Transform[] spawPosition, Level level)
        {
            for (int i = 0; i < level.numZombies; i++)
            {
                var spaw = spawPosition.RandomElement();
                Vector3 pos = spaw.position;
                Vector3 auxPos = spaw.position;
                level.InstantiateZombie(auxPos, Quaternion.identity);
            }
        }
    }
}