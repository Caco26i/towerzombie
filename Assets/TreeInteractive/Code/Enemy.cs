﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace TreeInteractive
{
    public class Enemy : MonoBehaviour
    {
        public float life = 1f;
        public bool isAttacking = false;

        private Animator _animator;
        private NavMeshAgent _agent;
        private CapsuleCollider _collider;
        private Rigidbody _rigidbody;
        // Start is called before the first frame update
        void Start()
        {
            _animator = GetComponent<Animator>();
            _agent = GetComponent<NavMeshAgent>();
            _collider = GetComponent<CapsuleCollider>();
            _rigidbody = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            if (life <= 0)
            {
                _animator.SetTrigger("Dead");
                _agent.enabled = false;
                _collider.enabled = false;
                _rigidbody.isKinematic = true;
            }
        }
    }
}