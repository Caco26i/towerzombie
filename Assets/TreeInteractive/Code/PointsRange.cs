﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeInteractive
{
    public class PointsRange : MonoBehaviour
    {
        public Tower tower;

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.tag == "Player")
            {
                var player = collider.gameObject.GetComponent<Player>();
                tower.life += player.health;
                tower.power += player.energy;

                player.health = 0;
                player.energy = 0;

                Destroy(player.actualFood);
            }
        }
    }
}