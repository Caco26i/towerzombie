﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeInteractive
{
    public class ShootingRange : MonoBehaviour
    {
        public Tower tower;

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("Enemy"))
            {
                tower.targets.Add(collider.GetComponent<Enemy>());
            }
        }
    }
}