﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TreeInteractive
{
    public class Tower : MonoBehaviour
    {
        [Range(0f, 1f)]
        public float life = 1;
        [Range(0f, 1f)]
        public float power = 1;
        public Slider lifeSlider;
        public Slider powerSlider;
        public float damageAmount = 0.05f;
        public List<Enemy> targets;

        public Enemy CurrentTarget
        {
            get { return (targets.Count > 0) ? targets[0] : null; }
        }
        //public Enemy actualtarget = null;

        
        private void Update()
        {
            CheckEnemiesLife();

            SetUISliders();
            ShootEnemy();
            CalculateDamageReceived();
        }

        private void ShootEnemy()
        {
            var currentTarget = CurrentTarget;
            if (currentTarget)
                currentTarget.life -= (Time.deltaTime * power) / 2.5f;
        }

        private void CheckEnemiesLife()
        {
            if (CurrentTarget)
            {
                if (CurrentTarget.life <= 0) targets.RemoveAt(0);

                power -= (Time.deltaTime * 0.05f) / 5;
            }
        }

        private void ReduceEnergy()
        {
            if (targets.Count > 0) power -= (Time.deltaTime * 0.05f) / 5;
        }

        private void SetUISliders()
        {
            lifeSlider.value = life;
            powerSlider.value = power;
        }

        private void CalculateDamageReceived()
        {
            int attacking = 0;
            foreach (var enemy in targets)
            {
                if (enemy.isAttacking) attacking++;
            }
            life -= (Time.deltaTime * attacking * damageAmount) / 5;
        }
    }
}