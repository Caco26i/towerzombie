﻿using UnityEngine;

namespace TreeInteractive
{
    public class PlayerMovement : MonoBehaviour
    {
        public int payerNumber = 1;
        public float speed = 12f;
        public float turnSpeed = 180f;

        private string _movementAxisName;
        private string _turnAxisName;
        private Rigidbody _rigidbody;
        private float _movementInputValue;
        private float _turnInputValue;
        private float _originalPitch;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void OnEnable()
        {
            _rigidbody.isKinematic = false;
            _movementInputValue = 0f;
            _turnInputValue = 0f;
        }

        private void OnDisable()
        {
            _rigidbody.isKinematic = true;
        }

        private void Start()
        {
            _movementAxisName = "Vertical" + payerNumber;
            _turnAxisName = "Horizontal" + payerNumber;
        }

        private void Update()
        {
            // Store the value of both input axes.
            _movementInputValue = Input.GetAxis(_movementAxisName);
            _turnInputValue = Input.GetAxis(_turnAxisName);
        }

        private void FixedUpdate()
        {
            Move();
            Turn();
        }

        // Adjust the position based on the player's input.
        private void Move()
        {
            Vector3 movement = transform.forward * _movementInputValue * speed * Time.deltaTime;

            _rigidbody.MovePosition(_rigidbody.position + movement);
        }

        // Adjust the rotation based on the player's input.
        private void Turn()
        {
            float turn = _turnInputValue * turnSpeed * Time.deltaTime;

            // Y-axis
            Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

            _rigidbody.MoveRotation(_rigidbody.rotation * turnRotation);
        }
    }
}