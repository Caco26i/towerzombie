﻿using UnityEngine;

namespace TreeInteractive.Util
{
    public static class ArrayExtension
    {
        public static T RandomElement<T>(this T[] list)
        {
            return list[Random.Range(0, list.Length)];
        }
    }
}
