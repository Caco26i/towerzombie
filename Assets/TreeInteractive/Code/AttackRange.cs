﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeInteractive
{
    public class AttackRange : MonoBehaviour
    {
        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.tag == "Enemy")
            {
                Enemy enemy = collider.gameObject.GetComponent<Enemy>();
                enemy.isAttacking = true;
            }
        }
    }
}